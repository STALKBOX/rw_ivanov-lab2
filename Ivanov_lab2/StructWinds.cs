﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivanov_lab2
{
    internal class StructWinds
    {
        public int MAX_FILE_ROWS_COUNT = 100;
        public int MAX_STRING_SIZE = 200;

        public struct Date
        {
            public int day;
            public int month;
        }

        public struct Winds
        {
            public string wind_direction;
            public double wind_speed;
        }
    }
}
