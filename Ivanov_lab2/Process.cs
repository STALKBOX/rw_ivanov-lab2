﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivanov_lab2
{
    public class Process
    {
        public double SredneeZnachenie(int mounth, double[,] arr)
        {
            int n = arr.GetLength(0);

            double sum = 0;
            int count = 0;

            for (int i = 0; i < n; i++)
            {
                if (arr[i, 0] == mounth)
                {
                    sum += arr[i, 1];
                    count++;
                }
            }
            return sum / count;
        }
    }
}
