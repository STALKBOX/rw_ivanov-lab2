﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivanov_lab2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Лабораторная работа №1. GIT");
            Console.WriteLine("Вариант №6. Роза ветров");
            Console.WriteLine("Автор: Влад Иванов");
            Console.WriteLine("Группа: 14з");

            Console.WriteLine("");

            string filename = "data.txt";
            int size = FileReader.SizeMas(filename);
            object[,] data_rw = new object[size, 4];

            //чтение информации из файла и запись в массив
            FileReader.Read(filename, data_rw);

            //вывод информации
            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine("|                  роза ветров                   |");
            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine("|  Дата   |  Направление ветра |  Скорость ветра |");
            Console.WriteLine("--------------------------------------------------");

            for (int i = 0; i < size; i++)
            {
                string day = data_rw[i, 0].ToString().PadLeft(2, '0');
                string mounth = data_rw[i, 1].ToString().PadLeft(2, '0');
                string direction = data_rw[i, 2].ToString();
                string speed = data_rw[i, 3].ToString();

                Console.WriteLine("|  " + day + "." + mounth + "   |   " + direction.PadRight(16, ' ')
                    + " |  " + speed.PadRight(13, ' ') + " |");
            }

            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine("");
            Console.WriteLine("");

            string filters;
            do
            {
                Console.Write("Выполнить фильтрацию данных? (yes/no): ");
                filters = Console.ReadLine();
                if (filters == "yes")
                {
                    Console.WriteLine("");
                    Console.WriteLine("Какую фильтрацию выполнить?");
                    Console.WriteLine("1) По направлению ветра");
                    Console.WriteLine("2) По скорости ветра выше 5 м/с");
                    Console.Write("Введите 1 или 2: ");
                    string n_filter = Console.ReadLine();

                    if (n_filter == "1")
                    {
                        Console.WriteLine("");
                        Console.Write("Выберите направление ветра (North, NorthEast, East, SouthEast, South, SouthWest, West, NorthWest): ");
                        string dw = Console.ReadLine();

                        if (dw == "North" || dw == "NorthEast" || dw == "East" || dw == "SouthEast"
                            || dw == "South" || dw == "SouthWest" || dw == "West" || dw == "NorthWest")
                        {
                            Console.WriteLine("--------------------------------------------------");
                            Console.WriteLine("|                Направление ветра               |");
                            Console.WriteLine("--------------------------------------------------");
                            Console.WriteLine("|  Дата   |  Направление ветра |  Скорость ветра |");
                            Console.WriteLine("--------------------------------------------------");

                            for (int i = 0; i < size; i++)
                            {
                                if (Convert.ToString(data_rw[i, 2]) == dw)
                                {
                                    string day = data_rw[i, 0].ToString().PadLeft(2, '0');
                                    string mounth = data_rw[i, 1].ToString().PadLeft(2, '0');
                                    string direction = data_rw[i, 2].ToString();
                                    string speed = data_rw[i, 3].ToString();

                                    Console.WriteLine("|  " + day + "." + mounth + "   |   " + direction.PadRight(16, ' ')
                                        + " |  " + speed.PadRight(13, ' ') + " |");
                                }
                            }
                            Console.WriteLine("--------------------------------------------------");
                            Console.WriteLine("");
                            Console.WriteLine("");
                        }
                        else
                        {
                            Console.WriteLine("Введено неверное значение");
                        }
                    }
                    else if (n_filter == "2")
                    {
                        Console.WriteLine("--------------------------------------------------");
                        Console.WriteLine("|           Скорость ветра больше 5м/с           |");
                        Console.WriteLine("--------------------------------------------------");
                        Console.WriteLine("|  Дата   |  Направление ветра |  Скорость ветра |");
                        Console.WriteLine("--------------------------------------------------");

                        for (int i = 0; i < size; i++)
                        {
                            if (Convert.ToDouble(data_rw[i, 3]) >= 5.0)
                            {
                                string day = data_rw[i, 0].ToString().PadLeft(2, '0');
                                string mounth = data_rw[i, 1].ToString().PadLeft(2, '0');
                                string direction = data_rw[i, 2].ToString();
                                string speed = data_rw[i, 3].ToString();

                                Console.WriteLine("|  " + day + "." + mounth + "   |   " + direction.PadRight(16, ' ')
                                    + " |  " + speed.PadRight(13, ' ') + " |");
                            }
                        }
                        Console.WriteLine("--------------------------------------------------");
                        Console.WriteLine("");
                        Console.WriteLine("");
                    }
                    else
                    {
                        Console.WriteLine("Команда не верная");
                    }
                }
                else if (filters != "yes" && filters != "no")
                {
                    Console.WriteLine("Команда не верная");
                }

            } while (filters != "no");

            Console.WriteLine("");
            Console.WriteLine("Нажмите любую клавишу для завершения");
            Console.ReadKey();

        }
    }
}
